﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeoAtlas.DAL
{
    public static class ClassHolderInitializer
    {
        public static void HolderSeed()
        {
            StaticClassHolder.Cities.Add(new Entity.CityEntity()
            {
                Name = "Kyiv",
                Region = "Miasto Kyiv"
            });
            StaticClassHolder.Countries.Add(new Entity.CountryEntity()
            {
                Name = "Ukraine",
                Capital = "Kyiv"
            });
            StaticClassHolder.Capitals.Add(new Entity.CapitalEntity()
            {
                Name="Kyiv",
                Country = "Ukraine"
            });
            StaticClassHolder.Memorials.Add(new Entity.MemorialEntity()
            {
                Name = "KPI",
                Location = "Kyiv"
            });
            StaticClassHolder.Nationalities.Add(new Entity.NationalityEntity()
            {
                Name = "laydaks",
                Number = 1488
            });
            StaticClassHolder.Regions.Add(new Entity.RegionEntity()
            {
                Name = "Khersonska oblast",
                Country = "Ukaine"
            });
            StaticClassHolder.SightningTypes.Add(new Entity.SightningTypeEntity()
            {
                TypeName = "painting"
            });
        }
    }
}
