﻿using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Abstract
{
    public interface ICountryRepository:IGenericRepository<CountryEntity>
    {
    }
}
