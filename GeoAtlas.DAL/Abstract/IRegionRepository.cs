﻿using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Abstract
{
    public interface IRegionRepository:IGenericRepository<RegionEntity>
    {
    }
}
