﻿using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Abstract
{
    public interface ICapitalRepository:IGenericRepository<CapitalEntity>
    {

    }
}
