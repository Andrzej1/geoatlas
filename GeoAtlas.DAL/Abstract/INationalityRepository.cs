﻿using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Abstract
{
    public interface INationalityRepository:IGenericRepository<NationalityEntity>
    {

    }
}
