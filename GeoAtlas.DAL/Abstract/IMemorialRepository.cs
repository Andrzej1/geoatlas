﻿using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Abstract
{
    public interface IMemorialRepository:IGenericRepository<MemorialEntity>
    {

    }
}
