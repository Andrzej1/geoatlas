﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Abstract
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        //Insert FetchAll fetchBy Update Remove
        TEntity Insert(TEntity entity);
        List<TEntity> FetchAll();
        List<TEntity> FetchBy(Expression<Func<TEntity, bool>> predicate);
        TEntity Update(TEntity entity);
        TEntity Remove(TEntity entity);
    }
}
