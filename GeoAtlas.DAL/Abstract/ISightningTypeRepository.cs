﻿using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Abstract
{
    public interface ISightningTypeRepository:IGenericRepository<SightningTypeEntity>
    {

    }
}
