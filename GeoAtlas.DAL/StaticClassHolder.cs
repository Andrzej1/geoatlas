﻿using GeoAtlas.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeoAtlas.DAL
{
    public static class StaticClassHolder
    {
        public static List<CityEntity> Cities { get; set; }
        public static List<CapitalEntity> Capitals { get; set; }
        public static List<CountryEntity> Countries { get; set; }
        public static List<MemorialEntity> Memorials { get; set; }
        public static List<NationalityEntity> Nationalities { get; set; }
        public static List<RegionEntity> Regions { get; set; }
        public static List<SightningTypeEntity> SightningTypes { get; set; }
        public static List<CountryNationalityEntity> CountryNationalities { get; set; }

        static StaticClassHolder()
        {
            Cities = new List<CityEntity>();
            Capitals = new List<CapitalEntity>();
            Countries = new List<CountryEntity>();
            Memorials = new List<MemorialEntity>();
            Nationalities = new List<NationalityEntity>();
            Regions = new List<RegionEntity>();
            SightningTypes = new List<SightningTypeEntity>();
            CountryNationalities = new List<CountryNationalityEntity>();
        }
    }
}
