﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using GeoAtlas.DAL.Abstract;
using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Implementation
{
    public class NationalityRepository : INationalityRepository
    {
        public List<NationalityEntity> FetchAll()
        {
            throw new NotImplementedException();
        }

        public List<NationalityEntity> FetchBy(Expression<Func<NationalityEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public NationalityEntity Insert(NationalityEntity entity)
        {
            throw new NotImplementedException();
        }

        public NationalityEntity Remove(NationalityEntity entity)
        {
            throw new NotImplementedException();
        }

        public NationalityEntity Update(NationalityEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
