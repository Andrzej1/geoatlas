﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using GeoAtlas.DAL.Abstract;
using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Implementation
{
    public class RegionRepository : IRegionRepository
    {
        public List<RegionEntity> FetchAll()
        {
            throw new NotImplementedException();
        }

        public List<RegionEntity> FetchBy(Expression<Func<RegionEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public RegionEntity Insert(RegionEntity entity)
        {
            throw new NotImplementedException();
        }

        public RegionEntity Remove(RegionEntity entity)
        {
            throw new NotImplementedException();
        }

        public RegionEntity Update(RegionEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
