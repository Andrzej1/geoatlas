﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using GeoAtlas.DAL.Abstract;
using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Implementation
{
    public class CityRepository : ICityRepository
    {
        public List<CityEntity> FetchAll()
        {
            throw new NotImplementedException();
        }

        public List<CityEntity> FetchBy(Expression<Func<CityEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public CityEntity Insert(CityEntity entity)
        {
            throw new NotImplementedException();
        }

        public CityEntity Remove(CityEntity entity)
        {
            throw new NotImplementedException();
        }

        public CityEntity Update(CityEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
