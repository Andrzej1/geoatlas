﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using GeoAtlas.DAL.Abstract;
using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Implementation
{
    public class CapitalRepository : ICapitalRepository
    {
        public List<CapitalEntity> FetchAll()
        {
            throw new NotImplementedException();
        }

        public List<CapitalEntity> FetchBy(Expression<Func<CapitalEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public CapitalEntity Insert(CapitalEntity entity)
        {
            throw new NotImplementedException();
        }

        public CapitalEntity Remove(CapitalEntity entity)
        {
            throw new NotImplementedException();
        }

        public CapitalEntity Update(CapitalEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
