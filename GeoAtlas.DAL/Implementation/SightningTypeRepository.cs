﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using GeoAtlas.DAL.Abstract;
using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Implementation
{
    public class SightningTypeRepository : ISightningTypeRepository
    {
        public List<SightningTypeEntity> FetchAll()
        {
            throw new NotImplementedException();
        }

        public List<SightningTypeEntity> FetchBy(Expression<Func<SightningTypeEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public SightningTypeEntity Insert(SightningTypeEntity entity)
        {
            throw new NotImplementedException();
        }

        public SightningTypeEntity Remove(SightningTypeEntity entity)
        {
            throw new NotImplementedException();
        }

        public SightningTypeEntity Update(SightningTypeEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
