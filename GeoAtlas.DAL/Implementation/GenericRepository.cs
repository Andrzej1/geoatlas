﻿using GeoAtlas.DAL.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using GeoAtlas.Entity;
using GeoAtlasCommon.Enums;

namespace GeoAtlas.DAL.Implementation
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        public TEntity Insert(TEntity entity)
        {
            Type t = typeof(StaticClassHolder);
            MethodInfo method = t.GetProperties().FirstOrDefault(p => p.MemberType.GetType() == typeof(List<TEntity>)).GetSetMethod();
            method.MakeGenericMethod(new Type[] { typeof(TEntity) });
            //entity.
            if (entity is IBaseEntity)
            {
                ((IBaseEntity)entity).Type = EntityStateType.Added;
            } 
            method.Invoke(entity, null);

            return entity;
        }


        public List<TEntity> FetchAll()
        {
            throw new NotImplementedException();
        }

        public List<TEntity> FetchBy(Expression<Func<TEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public TEntity Update(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public TEntity Remove(TEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
