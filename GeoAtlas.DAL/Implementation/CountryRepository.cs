﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using GeoAtlas.DAL.Abstract;
using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Implementation
{
    public class CountryRepository : ICountryRepository
    {
        public List<CountryEntity> FetchAll()
        {
            throw new NotImplementedException();
        }

        public List<CountryEntity> FetchBy(Expression<Func<CountryEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public CountryEntity Insert(CountryEntity entity)
        {
            throw new NotImplementedException();
        }

        public CountryEntity Remove(CountryEntity entity)
        {
            throw new NotImplementedException();
        }

        public CountryEntity Update(CountryEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
