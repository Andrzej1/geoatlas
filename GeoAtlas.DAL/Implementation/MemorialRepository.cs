﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using GeoAtlas.DAL.Abstract;
using GeoAtlas.Entity;

namespace GeoAtlas.DAL.Implementation
{
    public class MemorialRepository : IMemorialRepository
    {
        public List<MemorialEntity> FetchAll()
        {
            throw new NotImplementedException();
        }

        public List<MemorialEntity> FetchBy(Expression<Func<MemorialEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public MemorialEntity Insert(MemorialEntity entity)
        {
            throw new NotImplementedException();
        }

        public MemorialEntity Remove(MemorialEntity entity)
        {
            throw new NotImplementedException();
        }

        public MemorialEntity Update(MemorialEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
