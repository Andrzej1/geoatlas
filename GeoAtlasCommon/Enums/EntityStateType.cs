﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeoAtlasCommon.Enums
{
    public enum EntityStateType
    {
        NoChange = 1,
        Added,
        Updated,
        Removed
    }
}
