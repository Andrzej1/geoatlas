﻿using GeoAtlasCommon.Enums;

namespace GeoAtlas.Entity
{
    public class SightningTypeEntity : IBaseEntity
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
        public EntityStateType Type { get; set; }
        public int MemorialId { get; set; }
    }
}
