﻿using GeoAtlasCommon.Enums;
using System.Collections.Generic;

namespace GeoAtlas.Entity
{
    public class CountryEntity : IBaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Capital { get; set; }
        public EntityStateType Type { get; set; }
     //   public list<CityEntity> cities { get; set; }
   //     public virtual ICollection<CityEntity> Cities { get; set; }
        public int NationalityId { get; set; }
      
        public override string ToString()
        {
            return Name + "Capital:" + Capital;
        }
    }
}
