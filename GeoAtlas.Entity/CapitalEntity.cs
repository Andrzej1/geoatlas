﻿using System;
using GeoAtlasCommon.Enums;

namespace GeoAtlas.Entity
{
    public class CapitalEntity : IBaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }

        public EntityStateType Type { get; set; }

        public override string ToString()
        {
            return Name + "Capital of:" + Country;
        }
    }
}
