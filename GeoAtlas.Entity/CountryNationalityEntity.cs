﻿using GeoAtlasCommon.Enums;

namespace GeoAtlas.Entity
{
    public class CountryNationalityEntity:IBaseEntity
    {
        public int Id { get; set; }
        CountryEntity CountryId { get; set; }
        NationalityEntity NationalityId { get; set; }
        public EntityStateType Type { get; set; }

    }
}
