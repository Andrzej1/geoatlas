﻿using GeoAtlasCommon.Enums;

namespace GeoAtlas.Entity
{
    public class MemorialEntity : IBaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public EntityStateType Type { get; set; }
        public int CityId { get; set; }
        public int SightningTypeId { get; set; }
    }
}
