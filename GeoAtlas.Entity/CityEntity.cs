﻿using GeoAtlasCommon.Enums;

namespace GeoAtlas.Entity
{
    public class CityEntity : IBaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Region { get; set; }
        public EntityStateType Type { get; set; }
        public int CountryId { get; set; }
        public override string ToString()
        {
            return Name + "Located in:" + Region;
        }
    }
}
