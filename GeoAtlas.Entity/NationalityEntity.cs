﻿using GeoAtlasCommon.Enums;

namespace GeoAtlas.Entity
{
    public class NationalityEntity : IBaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public int CountryId { get; set; }
        public EntityStateType Type { get; set; }
    }
}
