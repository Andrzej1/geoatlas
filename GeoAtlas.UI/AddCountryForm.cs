﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GeoAtlas.DAL;

namespace GeoAtlas.UI
{
    public partial class AddCountryForm : Form
    {
        public AddCountryForm()
        {
            InitializeComponent();
        }
        MainForm MForm;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox1.Text) && !String.IsNullOrEmpty(textBox2.Text))
            {
                StaticClassHolder.Countries.Add(new Entity.CountryEntity()
                {
                    Id = StaticClassHolder.Countries.Count + 1,
                    Name = textBox1.Text,
                    Capital = textBox2.Text
                });
                MForm.lbCountriesRefresh();
            }
            else
            {
                Console.WriteLine("Write in all fields!");
            }
        }
    }
}
