﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GeoAtlas.DAL;
using GeoAtlas.Entity;

namespace GeoAtlas.UI
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddCityForm addCity = new AddCityForm();
            addCity.Show();
        }
        public void lbCitiesRefresh()
        {
            lbCities.DataSource = null;
            lbCities.DataSource = StaticClassHolder.Cities;
        }
        public void lbCountriesRefresh()
        {
            lbCountries.DataSource = null;
            lbCountries.DataSource = StaticClassHolder.Countries;
        }
        public void lbRegionsRefresh()
        {
            lbRegions.DataSource = null;
            lbRegions.DataSource = StaticClassHolder.Regions;
        }
        public void lbNationalitiesRefresh()
        {
            lbNationalities.DataSource = null;
            lbNationalities.DataSource = StaticClassHolder.Nationalities;
        }
        public void lbMemorialsRefresh()
        {
            lbMemorials.DataSource = null;
            lbMemorials.DataSource = StaticClassHolder.Memorials;
        }
        public void lbCapitalsRefresh()
        {
            lbCapitals.DataSource = null;
            lbCapitals.DataSource = StaticClassHolder.Capitals;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            StaticClassHolder.Cities.Remove((CityEntity)lbCities.SelectedItem);
        }
    }
}
