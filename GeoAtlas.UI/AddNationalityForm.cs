﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GeoAtlas.DAL;

namespace GeoAtlas.UI
{
    public partial class AddNationalityForm : Form
    {
        public AddNationalityForm()
        {
            InitializeComponent();
        }
        MainForm MForm;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox1.Text) && !String.IsNullOrEmpty(textBox2.Text))
            {
                StaticClassHolder.Nationalities.Add(new Entity.NationalityEntity()
                {
                    Id = StaticClassHolder.Capitals.Count + 1,
                    Name = textBox1.Text,
                    Number = Convert.ToInt32(textBox2.Text)
                });
                MForm.lbNationalitiesRefresh();
            }
            else
            {
                Console.WriteLine("Write in all fields!");
            }
        }
    }
}
