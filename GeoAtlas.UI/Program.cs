﻿using GeoAtlas.DAL.Abstract;
using GeoAtlas.DAL.Implementation;
using GeoAtlas.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using GeoAtlas.DAL;

namespace GeoAtlas.UI
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ClassHolderInitializer.HolderSeed();
            IGenericRepository<CountryEntity> rep = new GenericRepository<CountryEntity>();
            rep.Insert(new CountryEntity() { Id = 1000 });
            Application.Run(new MainForm());
        }
    }
}
