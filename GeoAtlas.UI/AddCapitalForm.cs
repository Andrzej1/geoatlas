﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GeoAtlas.DAL;

namespace GeoAtlas.UI
{
    public partial class AddCapitalForm : Form
    {
        public AddCapitalForm()
        {
            InitializeComponent();
        }

        MainForm MForm;

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox1.Text) && !String.IsNullOrEmpty(textBox2.Text))
            {
                StaticClassHolder.Capitals.Add(new Entity.CapitalEntity()
                {
                    Id = StaticClassHolder.Capitals.Count + 1,
                    Name = textBox1.Text,
                    Country = textBox2.Text
                });
                MForm.lbCapitalsRefresh();
            }
            else
            {
                Console.WriteLine("Write in all fields!");
            }
        }
    }
}
