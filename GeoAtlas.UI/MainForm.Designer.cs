﻿namespace GeoAtlas.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCities = new System.Windows.Forms.ListBox();
            this.lbRegions = new System.Windows.Forms.ListBox();
            this.lbNationalities = new System.Windows.Forms.ListBox();
            this.lbCapitals = new System.Windows.Forms.ListBox();
            this.lbCountries = new System.Windows.Forms.ListBox();
            this.lbMemorials = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.btnDelCity = new System.Windows.Forms.Button();
            this.btnDelRegion = new System.Windows.Forms.Button();
            this.btnDelNationality = new System.Windows.Forms.Button();
            this.btnDelCapital = new System.Windows.Forms.Button();
            this.btnDelMemorial = new System.Windows.Forms.Button();
            this.btnDelCountry = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbCities
            // 
            this.lbCities.FormattingEnabled = true;
            this.lbCities.ItemHeight = 20;
            this.lbCities.Location = new System.Drawing.Point(12, 85);
            this.lbCities.Name = "lbCities";
            this.lbCities.Size = new System.Drawing.Size(181, 184);
            this.lbCities.TabIndex = 0;
            // 
            // lbRegions
            // 
            this.lbRegions.FormattingEnabled = true;
            this.lbRegions.ItemHeight = 20;
            this.lbRegions.Location = new System.Drawing.Point(221, 85);
            this.lbRegions.Name = "lbRegions";
            this.lbRegions.Size = new System.Drawing.Size(181, 184);
            this.lbRegions.TabIndex = 1;
            // 
            // lbNationalities
            // 
            this.lbNationalities.FormattingEnabled = true;
            this.lbNationalities.ItemHeight = 20;
            this.lbNationalities.Location = new System.Drawing.Point(442, 85);
            this.lbNationalities.Name = "lbNationalities";
            this.lbNationalities.Size = new System.Drawing.Size(181, 184);
            this.lbNationalities.TabIndex = 2;
            // 
            // lbCapitals
            // 
            this.lbCapitals.FormattingEnabled = true;
            this.lbCapitals.ItemHeight = 20;
            this.lbCapitals.Location = new System.Drawing.Point(679, 85);
            this.lbCapitals.Name = "lbCapitals";
            this.lbCapitals.Size = new System.Drawing.Size(181, 184);
            this.lbCapitals.TabIndex = 3;
            // 
            // lbCountries
            // 
            this.lbCountries.FormattingEnabled = true;
            this.lbCountries.ItemHeight = 20;
            this.lbCountries.Location = new System.Drawing.Point(221, 334);
            this.lbCountries.Name = "lbCountries";
            this.lbCountries.Size = new System.Drawing.Size(181, 184);
            this.lbCountries.TabIndex = 4;
            // 
            // lbMemorials
            // 
            this.lbMemorials.FormattingEnabled = true;
            this.lbMemorials.ItemHeight = 20;
            this.lbMemorials.Location = new System.Drawing.Point(442, 334);
            this.lbMemorials.Name = "lbMemorials";
            this.lbMemorials.Size = new System.Drawing.Size(181, 184);
            this.lbMemorials.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 275);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 33);
            this.button1.TabIndex = 6;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(221, 275);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 33);
            this.button2.TabIndex = 7;
            this.button2.Text = "Add";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(442, 275);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(80, 33);
            this.button3.TabIndex = 8;
            this.button3.Text = "Add";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(679, 275);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(80, 33);
            this.button4.TabIndex = 9;
            this.button4.Text = "Add";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(221, 524);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(80, 33);
            this.button5.TabIndex = 10;
            this.button5.Text = "Add";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(442, 524);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(80, 33);
            this.button6.TabIndex = 11;
            this.button6.Text = "Add";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // btnDelCity
            // 
            this.btnDelCity.Location = new System.Drawing.Point(113, 275);
            this.btnDelCity.Name = "btnDelCity";
            this.btnDelCity.Size = new System.Drawing.Size(80, 33);
            this.btnDelCity.TabIndex = 12;
            this.btnDelCity.Text = "Del";
            this.btnDelCity.UseVisualStyleBackColor = true;
            this.btnDelCity.Click += new System.EventHandler(this.button7_Click);
            // 
            // btnDelRegion
            // 
            this.btnDelRegion.Location = new System.Drawing.Point(322, 275);
            this.btnDelRegion.Name = "btnDelRegion";
            this.btnDelRegion.Size = new System.Drawing.Size(80, 33);
            this.btnDelRegion.TabIndex = 13;
            this.btnDelRegion.Text = "Del";
            this.btnDelRegion.UseVisualStyleBackColor = true;
            // 
            // btnDelNationality
            // 
            this.btnDelNationality.Location = new System.Drawing.Point(543, 275);
            this.btnDelNationality.Name = "btnDelNationality";
            this.btnDelNationality.Size = new System.Drawing.Size(80, 33);
            this.btnDelNationality.TabIndex = 14;
            this.btnDelNationality.Text = "Del";
            this.btnDelNationality.UseVisualStyleBackColor = true;
            // 
            // btnDelCapital
            // 
            this.btnDelCapital.Location = new System.Drawing.Point(780, 275);
            this.btnDelCapital.Name = "btnDelCapital";
            this.btnDelCapital.Size = new System.Drawing.Size(80, 33);
            this.btnDelCapital.TabIndex = 15;
            this.btnDelCapital.Text = "Del";
            this.btnDelCapital.UseVisualStyleBackColor = true;
            // 
            // btnDelMemorial
            // 
            this.btnDelMemorial.Location = new System.Drawing.Point(543, 524);
            this.btnDelMemorial.Name = "btnDelMemorial";
            this.btnDelMemorial.Size = new System.Drawing.Size(80, 33);
            this.btnDelMemorial.TabIndex = 16;
            this.btnDelMemorial.Text = "Del";
            this.btnDelMemorial.UseVisualStyleBackColor = true;
            // 
            // btnDelCountry
            // 
            this.btnDelCountry.Location = new System.Drawing.Point(322, 524);
            this.btnDelCountry.Name = "btnDelCountry";
            this.btnDelCountry.Size = new System.Drawing.Size(80, 33);
            this.btnDelCountry.TabIndex = 17;
            this.btnDelCountry.Text = "Del";
            this.btnDelCountry.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 624);
            this.Controls.Add(this.btnDelCountry);
            this.Controls.Add(this.btnDelMemorial);
            this.Controls.Add(this.btnDelCapital);
            this.Controls.Add(this.btnDelNationality);
            this.Controls.Add(this.btnDelRegion);
            this.Controls.Add(this.btnDelCity);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbMemorials);
            this.Controls.Add(this.lbCountries);
            this.Controls.Add(this.lbCapitals);
            this.Controls.Add(this.lbNationalities);
            this.Controls.Add(this.lbRegions);
            this.Controls.Add(this.lbCities);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbCities;
        private System.Windows.Forms.ListBox lbRegions;
        private System.Windows.Forms.ListBox lbNationalities;
        private System.Windows.Forms.ListBox lbCapitals;
        private System.Windows.Forms.ListBox lbCountries;
        private System.Windows.Forms.ListBox lbMemorials;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button btnDelCity;
        private System.Windows.Forms.Button btnDelRegion;
        private System.Windows.Forms.Button btnDelNationality;
        private System.Windows.Forms.Button btnDelCapital;
        private System.Windows.Forms.Button btnDelMemorial;
        private System.Windows.Forms.Button btnDelCountry;
    }
}

