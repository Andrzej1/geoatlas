﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GeoAtlas.DAL;

namespace GeoAtlas.UI
{
    public partial class AddCityForm : Form
    {

        public AddCityForm()
        {
            InitializeComponent();
        }
        MainForm Mform;
        private void button1_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox1.Text) && !String.IsNullOrEmpty(textBox2.Text))
            {
                StaticClassHolder.Cities.Add(new Entity.CityEntity()
                {
                    Id = StaticClassHolder.Cities.Count+1,
                    Name = textBox1.Text,
                    Region = textBox2.Text
                });
                Mform.lbCitiesRefresh();
            }
            else
            {
                Console.WriteLine("Write in all fields!");
            }
        }
    }
}
