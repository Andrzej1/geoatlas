﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GeoAtlas.DAL;

namespace GeoAtlas.UI
{
    public partial class AddRegionForm : Form
    {
        public AddRegionForm()
        {
            InitializeComponent();
        }
        MainForm MForm;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox1.Text) && !String.IsNullOrEmpty(textBox2.Text))
            {
                StaticClassHolder.Regions.Add(new Entity.RegionEntity()
                {
                    Id = StaticClassHolder.Regions.Count + 1,
                    Name = textBox1.Text,
                    Country = textBox2.Text
                });
                MForm.lbRegionsRefresh();
            }
            else
            {
                Console.WriteLine("Write in all fields!");
            }
        }
    }
}
